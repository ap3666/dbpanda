from dbpanda import datastore
import pytest
import os


def test_datastore_00():
    with pytest.raises(ValueError):
        if 'DBPANDA_DATASET_LOC' in os.environ:
            del os.environ['DBPANDA_DATASET_LOC']
        db = datastore()
        assert db._datastore_available()


def test_datastore_01(tmpdir):
    db = datastore(loc=tmpdir)
    assert db._datastore_available()


def test_datastore_02(tmpdir):
    db = datastore(loc=tmpdir)
    assert os.path.exists(os.path.join(db._datastore, 'default'))


def test_datastore_03(tmpdir):
    db = datastore(loc=tmpdir)
    assert os.path.exists(os.path.join(db._datastore,
                          db._schema_config.config_filename))
