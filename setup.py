from setuptools import setup, find_packages

setup(
    name='dbpanda',
    version='0.0.1',
    packages=find_packages(include=['dbpanda', 'dbpanda.*']),
    install_requires=[
          'pandas',
      ],
)
