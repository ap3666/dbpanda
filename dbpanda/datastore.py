import os
import logging
from logging import config
import json

if os.getenv('LOGGING_CONFIG') is not None:
    config.fileConfig(os.getenv('LOGGING_CONFIG'))
logger = logging.getLogger(__name__)


class datastore_config:
    def __init__(self, datastore_location):
        self.datastore_location = datastore_location
        self.config_filename = 'schemas.json'
        self.schemas = dict()
        if self._config_available():
            self.load()
        self.dump()

    def dump(self):
        schema_config = self._get_schema_config_filename()
        logger.info('dumping data in config[%s]', schema_config)
        with open(schema_config, 'w') as f:
            json.dump(self.schemas, f)

    def _get_schema_config_filename(self):
        return os.path.join(self.datastore_location, self.config_filename)

    def _config_available(self):
        schema_config = self._get_schema_config_filename()
        if not os.path.exists(schema_config):
            logger.error('schema config[%s] not available', schema_config)
            return False
        logger.error('schema config[%s] is available', schema_config)
        return True

    def add_schema(self, name):
        logger.info('adding schema[%s] in datastore[%s]', name,
                    self.datastore_location)
        self.schemas[name] = dict()
        self.schemas[name]['name'] = name
        self.schemas[name]['schema_config'] = '{0}/datasets.json'.format(name)
        self.dump()

    def load(self):
        schema_config = self._get_schema_config_filename()
        with open(schema_config, 'r') as f:
            self.schemas = json.load(f)


class datastore:
    def __init__(self, loc=None):
        # Base parameters
        self._datastore = None
        self._schema_config = None
        # Checking datastore location
        if loc is not None:
            self._datastore = loc
            logger.debug('loc[%s] is available', loc)
        elif os.getenv('DBPANDA_DATASET_LOC') is not None:
            self._datastore = os.getenv('DBPANDA_DATASET_LOC')
            logger.info('DBPANDA_DATASET_LOC[%s] is available',
                        os.getenv('DBPANDA_DATASET_LOC'))
        else:
            logger.debug('datastore location is not provided. exiting...')
            raise ValueError('datastore location not suplied.')
        # after datastore location decided then setup config
        if not self._datastore_available():
            self._setup_datastore()
        if self._schema_config is None:
            self._schema_config = datastore_config(
                datastore_location=self._datastore)

    def _datastore_available(self):
        '''
            Check if datastore location is present in file system
        '''
        if os.path.exists(self._datastore):
            logger.info('datastore[%s] is available', self._datastore)
            if len(os.listdir(self._datastore)) == 0:
                logger.info('datastore[%s] directory only exists, no files',
                            self._datastore)
                return False
            return True
        logger.info('datastore[%s] is not available', self._datastore)
        return False

    def _setup_datastore(self):
        '''
            This function create initial setup before starting
            datastore
        '''
        logger.info('creating directory[%s]', self._datastore)
        os.makedirs(self._datastore, exist_ok=True)
        self._schema_config = datastore_config(
            datastore_location=self._datastore)
        self.add_schema(name='default')

    def _get_schema_location(self, name='default'):
        return os.path.join(self._datastore, name)

    def _schema_dir_available(self, name):
        if os.path.exists(self._get_schema_location(name=name)):
            logger.info('schema exists in datastore[%s]', self._datastore)
            return True
        return False

    def add_schema(self, name='default'):
        logger.info('adding schema in datatore[%s]', self._datastore)
        schema_loc = self._get_schema_location(name=name)
        if not self._schema_dir_available(name=name):
            os.makedirs(schema_loc)
            self._schema_config.add_schema(name=name)

    def check_schema(self, name, force_create=False):
        if not self._schema_dir_available(name=name):
            if force_create:
                self.add_schema(name=name)
                return True
            return False
        return True

    def remove(self):
        try:
            os.removedirs(self._datastore)
            logger.info('directory[%s] removed successfully', self._datastore)
        except NotADirectoryError:
            logger.error('path[%s] is not a directory', self._datastore)
        except PermissionError:
            logger.error('path[%s] permission denied', self._datastore)
        except OSError as error:
            logger.error('Error:', str(error))

    def location(self):
        return self._datastore


if __name__ == '__main__':
    db = datastore()
