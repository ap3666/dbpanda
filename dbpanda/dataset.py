import pandas as pd
import os
from .datastore import datastore


class dataset:
    def __init__(self,
                 name,
                 schema=None,
                 datastore_loc=None,
                 force_create=False):
        self._name = name
        self._ds = datastore(loc=datastore_loc)
        self._schema = self._find_schema(schema,
                                         force_create=force_create)
        self._create_dataset()

    def _find_schema(self, schema,
                     force_create=False):
        if self._ds.check_schema(name=schema,
                                 force_create=force_create):
            return schema
        return 'default'

    def _get_dataset_location(self):
        return os.path.join(self._ds.location(), self._schema, self._name)

    def _dataset_available(self):
        return os.path.exists(self._get_dataset_location())

    def _create_dataset(self):
        if not self._dataset_available():
            os.makedirs(self._get_dataset_location())

    def add_data(self, df):
        # [TODO] replace file which handles data locations when data is big
        self.data_location = os.path.join(self._get_dataset_location(),
                                          'data.h5')
        if os.path.isfile(self.data_location):
            df_out = pd.read_hdf(self.data_location, key='data')
            os.remove(self.data_location)
            df_out.append(df)
        else:
            df_out = df
        df_out.drop_duplicates(inplace=True)
        df_out.to_hdf(self.data_location, key='data')

    def read(self):
        self.data_location = os.path.join(self._get_dataset_location(),
                                          'data.h5')
        df_out = pd.read_hdf(self.data_location, key='data')
        return df_out


if __name__ == "__main__":
    ds = dataset(name='price_1m')
    df = ds.read()
    print(df.head())
